import { For, type Component } from 'solid-js';
import { A } from '@solidjs/router';

import BubbleSvg from "../../components/icons/BubbleSvg";
import { SORTING_ROUTE } from '../../routing/routes';



const homePageItems = [
    { media: BubbleSvg, title: "Sorting", route: SORTING_ROUTE },
];


const HomePage: Component = () => {
     const list = Array.from({ length: 5 }, (_, i) => i + 1);

  return (
        <div class=''>
            <ul class='grid grid-cols-2 gap-4 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 list-none'>
                <For each={homePageItems}>
                    {({ media: MediaSvg, route, title }) => (
                        <li class='flex justify-center items-center w-full bg-pinkBright-300 p-6 border border-gray-200 rounded-lg shadow hover:bg-pinkBright-100'>
                            <div class='text-ghostWhite font-bold'>
                                <A href={route}>
                                    <MediaSvg width={100} height={100} />
                                </A>
                                <A href={route}>
                                    {title}
                                </A>
                            </div>
                        </li>
                    )}
                </For>
            </ul>
        </div>
  );
};

export default HomePage;
