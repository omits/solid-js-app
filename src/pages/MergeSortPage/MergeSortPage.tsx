import type { Component } from 'solid-js';

import SortContextProvider from '../../context/SortContext';
import MergeSortContainer from '../../components/feature/MergeSort/MergeSortContainer';
import mergeSortAlgorithm from '../../components/feature/MergeSort/mergeSortAlgorithm';

const MergeSortPage: Component = () => (
    <SortContextProvider algorithm={mergeSortAlgorithm}>
        <MergeSortContainer />
    </SortContextProvider>
);

export default MergeSortPage;
