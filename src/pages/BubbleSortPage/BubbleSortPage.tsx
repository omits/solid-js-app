import type { Component } from 'solid-js';

import BubbleSortContainer from '../../components/feature/BubbleSort/BubbleSortContainer';
import SortContextProvider from '../../context/SortContext';
import bubleSortAlgorithm from '../../components/feature/BubbleSort/bubleSortAlgorithm';

const BubbleSortPage: Component = () => (
  <SortContextProvider algorithm={bubleSortAlgorithm}>
    <BubbleSortContainer />
  </SortContextProvider>
);

export default BubbleSortPage;
