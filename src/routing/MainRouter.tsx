import { Router, Route } from '@solidjs/router';
import { lazy, Suspense } from 'solid-js';
import type { Component } from 'solid-js';

import MainLayout from './MainLayout';
import { wait } from '../utils/general';
import { SORTING_ROUTE, MERGE_SORT_ROUTE, BUBBLE_SORT_ROUTE } from './routes';

const HomePage = lazy(async () => {
  await wait(100);
  return import('../pages/HomePage');
});

const BubbleSortPage = lazy(async () => {
  await wait(100);
  return import('../pages/BubbleSortPage');
});

const MergeSortPage = lazy(async () => {
  await wait(100);
  return import('../pages/MergeSortPage');
});

const MainRouter: Component = () => (
    <Router root={MainLayout}>
        <Route path="/" component={() => (
            <Suspense fallback="Loading">
            <HomePage />
        </Suspense>
        )} />
        <Route path={SORTING_ROUTE}>
            <Route path={BUBBLE_SORT_ROUTE} component={() => (
                <Suspense fallback="...loading">
                    <BubbleSortPage />
                </Suspense>
            )} />
            <Route path={MERGE_SORT_ROUTE} component={() => (
                <Suspense fallback="...loading">
                    <MergeSortPage />
                </Suspense>
            )} />
        </Route>
    </Router>
);

export default MainRouter;
