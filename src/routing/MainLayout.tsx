import type { Component, ParentProps } from 'solid-js';

import Header from '../components/common/Header';
import { HOME_ROUTE, SORTING_ROUTE } from './routes';

const links = [
  {
    title: 'Home',
    route: HOME_ROUTE,
  },
  {
    title: 'Sorting',
    route: SORTING_ROUTE,
  },
  // {
  //   title: 'Bubble Sort',
  //   route: BUBBLE_SORT_ROUTE,
  // },
  // {
  //   title: 'Merge Sort',
  //   route: MERGE_SORT_ROUTE,
  // },
];

const MainLayout: Component = (props: ParentProps) => (
    <>
        <Header links={links} />
        <main class="container mx-auto px-4">
            {props.children}
        </main>
    </>
);

export default MainLayout;
