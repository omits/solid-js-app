export const HOME_ROUTE = '/';

export const SORTING_ROUTE = '/sorting';
export const BUBBLE_SORT_ROUTE = '/bubble-sort';
export const MERGE_SORT_ROUTE = '/merge-sort';
