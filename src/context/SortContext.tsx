import { createContext } from 'solid-js';
import type { Component, ParentProps } from 'solid-js';
import { createStore } from 'solid-js/store';

export type SortingCb = (result: number[], idx1: number, idx2: number) => void;

type Options = {
  leftIdx: number;
  rightIdx: number;
};

export type SortingAlgorithmFn = (
  data: number[],
  cb: SortingCb,
  options?: Options,
) => Promise<number[]>;

type StoreType = {
  data: number[],
  arrayLength: number,
};

export type SortContextType = [
  StoreType,
  {
    setArrayLength?: (len: number) => void,
    setArray?: (data: number[]) => void,
    sortArray?: (cb: SortingCb) => void,
  },
];

export const SortContext = createContext<SortContextType>([
  { data: [], arrayLength: 3 },
  {},
]);

export const SortContextProvider: Component<{ algorithm: SortingAlgorithmFn } & ParentProps> = (
  props,
) => {
  const [state, setState] = createStore<StoreType>({ data: [], arrayLength: 3 });

  const sortContextValue: SortContextType = [
    state,
    {
      setArrayLength(len: number) {
        setState('arrayLength', len);
      },
      setArray(data: number[]) {
        setState('data', data);
      },
      sortArray: (cb) => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        props.algorithm(state.data, cb);
      },
    },
  ];

  return (
    <SortContext.Provider value={sortContextValue}>
        {props.children}
    </SortContext.Provider>
  );
};

export default SortContextProvider;
