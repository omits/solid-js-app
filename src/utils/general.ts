export const wait = (t: number) => new Promise((r) => {
  setTimeout(r, t);
});

const random = (min: number, max: number): number => Math.round(min + Math.random() * (max - min));

type GenerateArrayOptions = {
  length?: number;
  min?: number;
  max?: number;
};

export const generateArrayOfNumbers = ({
  min = 0,
  max = 100,
  length = 10,
}: GenerateArrayOptions): number[] => {
  const result = [];

  for (let i = 0; i < length; i += 1) {
    result[i] = random(min, max);
  }

  return result;
};

export const generateArrayOfConsecutive = ({
  min = 0,
  max = 10,
}: GenerateArrayOptions): number[] => {
  const result = [];

  for (let num = min; num <= max; num += 1) {
    result[result.length] = num;
  }

  return result;
};

/**
 * Linear interpolation
 */
export const scaleValue = (
  originalValue: number,
  originalMin: number,
  originalMax: number,
  targetMin: number,
  targetMax: number,
): number => (
  targetMin + (
    ((originalValue - originalMin)
    * (targetMax - targetMin))
    / (originalMax - originalMin))
);

export const swap = (arr: number[], idx1: number, idx2:number) => {
  // eslint-disable-next-line no-param-reassign
  [arr[idx1], arr[idx2]] = [arr[idx2], arr[idx1]];
};
