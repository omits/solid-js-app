import type { Component } from 'solid-js';
import 'flowbite';

import MainRouter from './routing/MainRouter';

const App: Component = () => (
    <MainRouter />
);

export default App;
