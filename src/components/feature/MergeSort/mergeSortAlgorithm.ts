import type { SortingAlgorithmFn } from '../../../context/SortContext';
import { wait } from '../../../utils/general';
// import { swap, wait } from '../../../utils/general';

const merge = (arr1: number[], arr2: number[]) => {
  const result = [];

  let x1 = 0;
  let x2 = 0;

  while (x1 < arr1.length && x2 < arr2.length) {
    const val1 = arr1[x1];
    const val2 = arr2[x2];

    if (val1 < val2) {
      result.push(val1);
      x1 += 1;
    } else {
      result.push(val2);
      x2 += 1;
    }
  }

  while (x1 < arr1.length) {
    result.push(arr1[x1]);
    x1 += 1;
  }

  while (x2 < arr2.length) {
    result.push(arr2[x2]);
    x2 += 1;
  }

  return result;
};

const mergeSortAlgorithm: SortingAlgorithmFn = async (
  data,
  sortingCallback,
  options,
) => {
  if (data.length < 2) return data;

  const mid = Math.round(data.length / 2);
  const leftSide = await mergeSortAlgorithm(
    data.slice(0, mid),
    sortingCallback,
    { leftIdx: 0, rightIdx: mid - 1 },
  );
  const rightSide = await mergeSortAlgorithm(
    data.slice(mid, data.length),
    sortingCallback,
    { leftIdx: mid, rightIdx: data.length - 1 },
  );

  const result = merge(leftSide, rightSide);

  await wait(300);
  sortingCallback(
    result,
    options?.leftIdx || 0,
    options?.rightIdx || result.length,
  );

  return result;
};

export default mergeSortAlgorithm;
