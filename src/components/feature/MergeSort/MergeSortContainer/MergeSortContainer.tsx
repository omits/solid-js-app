import { useContext, type Component } from 'solid-js';

import Input from '../../../common/Input';
import Button from '../../../common/Button';
import { generateArrayOfNumbers } from '../../../../utils/general';
import Chart from '../../../common/Chart';
import { SortContext } from '../../../../context/SortContext';
import type { SortContextType } from '../../../../context/SortContext';

const MergeSortContainer: Component = () => {
  const [state, { setArrayLength, sortArray, setArray }] = useContext<SortContextType>(SortContext);

  const handleSetArrayLength = (_: Event, value: string | number | undefined) => {
    if (value) {
      setArrayLength?.(Number(value));
    }
  };

  const handleGenerateArray = () => {
    setArray?.(
      generateArrayOfNumbers({ length: state.arrayLength }),
    );
  };

  const cb = (result: number[], idx1: number, idx2: number) => {
    const newArray = [...state.data];
    newArray.splice(idx1, result.length, ...result);
    console.log('asdf', state.data, newArray, idx1, idx2, result);

    setArray?.(newArray);
  };

  const handleSortArray = () => {
    sortArray?.(cb);
  };

  return (
        <>
          <div class='flex flex-col gap-2'>
              <Input
                  label="Set an array length"
                  value={state.arrayLength}
                  onChange={handleSetArrayLength}
                  onKeyDown={(e) => {
                    if (e.key === 'Enter') {
                      handleGenerateArray();
                    }
                  }}
              />
              <div class='flex flex-row gap-2'>
                <Button variant='outlined' onClick={handleGenerateArray}>
                  Generate an array
                </Button>
                <Button variant='contained' onClick={handleSortArray} disabled={state.arrayLength < 2}>
                  Sort it
                </Button>
              </div>
          </div>
          {state.data.map((i) => (
            <span>[{i}]</span>
          ))}
          <Chart
            classList={{ container: 'mt-2 border-2 border-gray-100' }}
            data={state.data}
          />
        </>
  );
};

export default MergeSortContainer;
