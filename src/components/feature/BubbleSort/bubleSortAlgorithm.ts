import type { SortingAlgorithmFn } from '../../../context/SortContext';
import { swap, wait } from '../../../utils/general';

const bubleSortAlgorithm: SortingAlgorithmFn = async (
  data,
  sortingCallback,
) => {
  const result = [...data];
  let noSwaps: boolean;

  for (let i = result.length; i > 0; i -= 1) {
    noSwaps = true;
    for (let j = 0; j < i - 1; j += 1) {
      if (result[j] > result[j + 1]) {
        // eslint-disable-next-line no-await-in-loop
        await wait(10);

        swap(result, j, j + 1);
        sortingCallback?.([...result], j, j + 1);

        noSwaps = false;
      }
    }
    if (noSwaps) break;
  }

  return result;
};

export default bubleSortAlgorithm;
