import {
  Index, createMemo, createSignal, onCleanup, onMount,
} from 'solid-js';
import Bar from './Bar';

type ChartProps = {
  data: number[];
  classList?: {
    container?: string;
  }
};

const minBarHeight = 10;
const maxBarHeight = 300;
const defaultClasses = 'w-full h-[300px] relative flex flex-col justify-center items-center grow overflow-hidden';

const Chart = (props: ChartProps) => {
  let container: HTMLDivElement | undefined;

  const [containerWidth, setContainerWidth] = createSignal<number>(0);

  const maxPoint = createMemo(() => Math.max(...props.data));
  const minPoint = createMemo(() => Math.min(...props.data));

  const handleSetContainerWidth = () => {
    if (container) {
      setContainerWidth(container.clientWidth);
    }
  };

  onMount(() => {
    handleSetContainerWidth();
  });

  onMount(() => {
    window.addEventListener('resize', handleSetContainerWidth);
  });

  onCleanup(() => {
    window.removeEventListener('resize', handleSetContainerWidth);
  });

  return (
    <div
        ref={container}
        class={`${defaultClasses} ${props.classList?.container || ''}`}
    >
        <svg class="w-full h-full">
            <g>
                <Index each={props.data}>
                    {(item, index) => (
                        <Bar
                            currentValue={item()}
                            currentIndex={index}
                            totalNumberOfValues={props.data.length}
                            minValue={minPoint()}
                            maxValue={maxPoint()}
                            minBarHeight={minBarHeight}
                            maxBarHeight={maxBarHeight}
                            containerWidth={containerWidth()}
                        />
                    )}
                </Index>
            </g>
        </svg>
    </div>
  );
};

export default Chart;
