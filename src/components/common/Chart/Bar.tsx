import { createMemo } from 'solid-js';

import { scaleValue } from '../../../utils/general';

type BarType = {
  currentValue: number;
  currentIndex: number;
  totalNumberOfValues: number;
  minValue: number;
  maxValue: number;
  minBarHeight: number;
  maxBarHeight: number;
  containerWidth: number;
};

const gap = 5;

const Bar = (props: BarType) => {
  const height = createMemo(() => scaleValue(
    props.currentValue,
    props.minValue,
    props.maxValue,
    props.minBarHeight,
    props.maxBarHeight,
  ));

  const width = createMemo(() => Math.max(
    (props.containerWidth / props.totalNumberOfValues) - gap,
    1,
  ));

  const translateX = createMemo(() => (
    (props.containerWidth / props.totalNumberOfValues) > gap
      ? (props.currentIndex * ((props.containerWidth / props.totalNumberOfValues)) + gap)
      : (props.currentIndex * ((props.containerWidth / props.totalNumberOfValues)) + 1)
  ));

  const translateY = createMemo(() => props.maxBarHeight - height());

  return (
    <rect
      data-bar-value={props.currentValue}
      data-bar-index={props.currentIndex}
      cursor="unset"
      class="stroke-none fill-emerald-600 opacity-100 transition-all duration-75"
      style={{
        height: `${height()}px`,
        width: `${width()}px`,
        transform: `translate3d(${translateX()}px, ${translateY()}px, 0px)`,
      }}
    />
  );
};

export default Bar;
