import { createUniqueId } from 'solid-js';
import type { Component, JSX, ParentProps } from 'solid-js';

type ButtonVariantType = 'outlined' | 'contained';

type ButtonProps = {
  onClick: () => void;
  variant?: ButtonVariantType;
  fullWidth?: boolean;
  disabled?: boolean;
};

const Button: Component<ButtonProps & ParentProps & JSX.HTMLAttributes<HTMLButtonElement>> = (
  props,
) => {
  const id = createUniqueId();

  const handleClick = () => {
    props.onClick();
  };

  return (
    <button
      id={id}
      {...props}
      class={props.class
        ?? 'text-sm text-center font-semibold py-2 px-4 rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300'}
      classList={{
        'bg-transparent hover:bg-blue-500 text-blue-700 hover:text-white border border-blue-500 hover:border-transparent': props.variant === 'outlined',
        'bg-blue-500 hover:bg-blue-700 text-white dark:bg-blue-600 dark:hover:bg-blue-800 dark:focus:ring-blue-800': props.variant === 'contained',
        'opacity-75 pointer-events-none text-stone-700 border-stone-500': props.disabled && props.variant === 'outlined',
        'opacity-75 pointer-events-none bg-stone-300 border-stone-500': props.disabled && props.variant === 'contained',
        'w-fit': !props.fullWidth,
        'w-full': props.fullWidth,
      }}
      onClick={handleClick}
      >
        {props.children}
    </button>
  );
};

export default Button;
