import type { Component, ParentProps } from 'solid-js';
import { Portal } from 'solid-js/web';

type ModalProps = {
  open: boolean;
};

const Modal: Component<ModalProps & ParentProps> = (props) => (
    <>
        {props.open && (
            <Portal>
                {props.children}
            </Portal>
        )}
    </>
);

export default Modal;
